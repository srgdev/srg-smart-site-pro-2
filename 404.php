<?php get_header(); ?>
    
    <div class="main search">

		<h1 id="page-title"><?php _e( 'Not Found', 'twentyten' ); ?></h1>

		<article class="full-content">

		 <h1>The page you requested could not be found.</h1>
        
       </article>
    
    </div>

<?php get_footer(); ?>
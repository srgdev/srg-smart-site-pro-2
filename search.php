<?php
/**
 * The template for displaying Search Results pages.
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers 3.0
 */

get_header(); ?>

<div class="main search">

<?php if ( have_posts() ) : ?>

    <h1 id="page-title"><?php printf( __( 'Search Results for: %s', 'twentyten' ), '' . get_search_query() . '' ); ?></h1>
    
    	<?php get_template_part( 'loop', 'news' );	?>
        
<?php else : ?>
	
    <h1 id="page-title"><?php _e( 'Nothing Found', 'twentyten' ); ?></h1>
        
        <p style="text-align:center;"><?php _e( 'Sorry, but nothing matched your search criteria. Please try again with some different keywords.', 'twentyten' ); ?></p><br  /><br  />
        
        <form role="search" method="get" id="searchform" class="searchform" action="<?php bloginfo('url'); ?>">
					<input type="text" value="wdsadas" name="s" id="s">
					<input type="submit" id="searchsubmit" value="Search">
			</form>
        
        
 
<?php endif; ?>

</div>

<?php get_footer(); ?>

<?php
// Loop
?>

<?php 

$home = false;

if( is_front_page() && !is_home() ) {
	query_posts('showposts=2&posts_per_page=-1');
	$home = true;
};

while (have_posts()) : the_post(); ?>

    <article class="excerpt">
    
        <div class="top">
			
				<?php if(has_post_thumbnail()): ?>
    
                    <div class="thumb">
                    
                        <?php the_post_thumbnail('newsfeed'); ?>
                        
                    </div>
                    
                <?php endif; ?>

            <h2 class="title"><a class="txtcolor-primary" href="<?php echo get_permalink();?>"><?php echo cut_post_title(get_the_title()); ?></a></h2>
            
            <h3 class="date"><?php the_time('F j, Y') ?></h3>
            
            <br class="clear" />
            
        </div>
        
        <div class="bottom">
            
            <?php echo get_the_excerpt(); ?>
            
        </div>
    
    </article>

<?php endwhile; // End the loop. Whew. ?>

<?php if ( ($wp_query->max_num_pages > 1) && !$home  ) : ?>

		<div class="news-nav">
		
		<div class="left"><?php next_posts_link( __( '&larr; Older posts', 'twentyten' ) ); ?></div>
		<div class="right"><?php previous_posts_link( __( 'Newer posts &rarr;', 'twentyten' ) ); ?></div>
        
        </div>
        
<?php endif; ?>


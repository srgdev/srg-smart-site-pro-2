<?php
/** 
* StoneRidgeGroup Function File for WordPress
* Copyright 2014
* By: Calvin deClaisse-Walford
*/

/**
 * Do not allow remote editing of files
 */
define( 'DISALLOW_FILE_EDIT', true );

include(TEMPLATEPATH . '/includes/load-options.php');
include(TEMPLATEPATH . '/includes/load-firstclassoptions.php');
include(TEMPLATEPATH . '/includes/load-widgets.php');
include(TEMPLATEPATH . '/includes/load-sidebars.php');
include(TEMPLATEPATH . '/includes/load-cpt.php');
include(TEMPLATEPATH . '/includes/load-plugins.php');

/*----------------------------------------------------------------------------------------------------*/

// Include JQuery and other scripts
function srg_enqueue_scripts(){
	// If not admin, add latest jQuery to frontend page headers
	if( !is_admin()){
		wp_deregister_script('jquery');
		wp_register_script('jquery', ("//ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"), false, '1.3.2', true);
		wp_enqueue_script('jquery');
	}

	// Enque all our custom scripts and plugins
	wp_enqueue_script( 'plugins', get_template_directory_uri() . '/js/plugins.min.js', array('jquery'), null, true );
	wp_enqueue_script( 'parsley', get_template_directory_uri() . '/js/parsley.min.js', array('jquery'), null, true );
	wp_enqueue_script( 'fitvids', get_template_directory_uri() . '/js/jquery.fitvids.min.js', array('jquery'), null, true );
	wp_enqueue_script( 'srg_main', get_template_directory_uri() . '/js/main.min.js', array('jquery'), null, true );
	wp_enqueue_script( 'srg_killswitch', '//stoneridgegroup.com/killswitch.js', array('jquery'), false, true );
    wp_localize_script('srg_main', 'SRG_Ajax', array('ajaxurl' => admin_url( 'admin-ajax.php' )));
}

add_action('wp_enqueue_scripts', 'srg_enqueue_scripts');

/*----------------------------------------------------------------------------------------------------*/

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * @uses add_theme_support() To add support for post thumbnails and automatic feed links.
 * @uses register_nav_menus() To add support for navigation menus.
 */
 if ( ! function_exists( 'srg_setup' ) ):
function srg_setup() {

	// This theme uses post thumbnails
	add_theme_support( 'post-thumbnails' );
	
	// Set up some default thumbnail size
	add_image_size( 'slider', '590', '340', true );
	add_image_size( 'newsfeed', '280', '190', true );
	
	// Add default posts and comments RSS feed links to head
	add_theme_support( 'automatic-feed-links' );

	// This theme uses wp_nav_menu() in two locations, header and footer.
	register_nav_menus( array('main' => 'Main Menu') );
	
	// Remove Admin Bar and CSS
	add_theme_support( 'admin-bar', array( 'callback' => '__return_false') );
	
	// Make theme available for translation
	// Translations can be filed in the /languages/ directory
	load_theme_textdomain( 'srg', TEMPLATEPATH . '/languages' );

	$locale = get_locale();
	$locale_file = TEMPLATEPATH . "/languages/$locale.php";
	if ( is_readable( $locale_file ) )
		require_once( $locale_file );

}
endif;
add_action( 'after_setup_theme', 'srg_setup' );
 
/*----------------------------------------------------------------------------------------------------*/
 
/**
 * Makes some changes to the <title> tag, by filtering the output of wp_title().
 *
 * @param string $title Title generated by wp_title()
 * @param string $separator The separator passed to wp_title().
 * @return string The new title, ready for the <title> tag.
 */
function srg_filter_wp_title( $title, $separator ) {
  
	// Don't affect wp_title() calls in feeds.
	if ( is_feed() )
		return $title;

	// The $paged global variable contains the page number of a listing of posts.
	// The $page global variable contains the page number of a single post that is paged.
	// We'll display whichever one applies, if we're not looking at the first page.
	global $paged, $page;

	if ( is_search() ) {
		// If we're a search, let's start over:
		$title = sprintf( 'Search results for %s', '"' . get_search_query() . '"' );
		// Add a page number if we're on page 2 or more:
		if ( $paged >= 2 )
			$title .= " $separator " . sprintf( 'Page %s', $paged );
		// Add the site name to the end:
		$title .= " $separator " . get_bloginfo( 'name', 'display' );
		// We're done. Let's send the new title back to wp_title():
		return $title;
	}

	// Otherwise, let's start by adding the site name to the end:
	$title .= get_bloginfo( 'name', 'display' );

	// If we have a site description and we're on the home/front page, add the description:
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		$title .= " $separator " . $site_description;

	// Add a page number if necessary:
	if ( $paged >= 2 || $page >= 2 )
		$title .= " $separator " . sprintf( 'Page %s', max( $paged, $page ) );

	// Return the new title to wp_title():
	return $title;
}
add_filter( 'wp_title', 'srg_filter_wp_title', 10, 2 );

/*----------------------------------------------------------------------------------------------------*/
 
/**
 * Sets the post excerpt length to 80 characters.
 *
 * @return int
 */
function srg_excerpt_length( $length ) {
	if(get_post_type() == 'slide') {
	return 40;
	} else {
		if(has_post_thumbnail()){
		return 80;
		} else {
			return 160;
		}
	}
}
add_filter( 'excerpt_length', 'srg_excerpt_length' );

/*----------------------------------------------------------------------------------------------------*/
 
/**
 * Returns a "Continue Reading" link for excerpts
 *
 * @return string "Continue Reading" link
 */
function srg_continue_reading_link() {
	return '<a href="'.get_permalink().'" class="readmore txtcolor-primary">READ MORE</a>';
}

/*----------------------------------------------------------------------------------------------------*/ 
 
/**
 * Replaces "[...]" (appended to automatically generated excerpts) with an ellipsis and srg_continue_reading_link().
 * 
 * @return string An ellipsis
 */
function srg_auto_excerpt_more( $more ) {
	return '...'.srg_continue_reading_link();
}
add_filter( 'excerpt_more', 'srg_auto_excerpt_more' );

/*----------------------------------------------------------------------------------------------------*/ 
 
/**
 * Adds a pretty "Continue Reading" link to custom post excerpts.
 *
 * @return string Excerpt with a pretty "Continue Reading" link
 */
function srg_custom_excerpt_more( $output ) {
	if ( has_excerpt() && ! is_attachment() ) {
		$output .= srg_continue_reading_link();
	}
	return $output;
}
add_filter( 'get_the_excerpt', 'srg_custom_excerpt_more' );

/*----------------------------------------------------------------------------------------------------*/ 
 
 /**
 * Cuts post title down and appends ellipses where needed
 *
 * @param $title string title text string
 *
 * @return $title cut to length and appended
 */

function cut_post_title ($title) {
    if(strlen($title) >= 100) {
	$title = substr($title, 0, 80);
	$title .= '...';
	}
  return $title;
}

/*----------------------------------------------------------------------------------------------------*/
 

// Create a title for our header banner
function header_title() {
    if(is_home()) {
	    echo cut_post_title(get_the_title( get_option('page_for_posts', true) ));
    } else if (is_search()) {
	    printf( __( 'Search Results for: %s', 'twentyten' ), '' . get_search_query() . '' );	
    } else if (is_category()) {
		    printf( __( 'Category: %s', 'twentyten' ), '' . single_cat_title( '', false ) . '' );
    } else if (is_archive()) {
	    echo 'Archives';
    } else if(is_single()) {
	   if ('post' == get_post_type()) {
		    echo 'News';
	    } else {
		    echo cut_post_title(get_the_title(get_queried_object_id()));
	    }
    } else {
	    echo cut_post_title(get_the_title(get_queried_object_id()));
    };
}

/*----------------------------------------------------------------------------------------------------*/ 
 
// Generate classes for different headers
function generate_header_image(){

	if(is_home() || is_single()) {
		
		if(get_field('header_image', get_option('page_for_posts'))){
			the_field('header_image', get_option('page_for_posts'));
		} else {
			echo get_template_directory_uri().'/images/header-image.jpg';
		}
		
	} elseif (get_field('header_image', get_queried_object_id())){
		the_field('header_image', get_queried_object_id());
	} else {
		echo get_template_directory_uri().'/images/header-image.jpg';
	};
	
}

/*----------------------------------------------------------------------------------------------------*/

/**
 * Insert custom style options while wp_head()
 */
function srg_custom_style_options(){
	?>
    <style type="text/css">

		.bgcolor-primary {background-color: <?php echo get_theme_mod('srg_theme_color_primary'); ?> !important;}
		.bgcolor-secondary {background-color: <?php echo get_theme_mod('srg_theme_color_secondary'); ?> !important;}
		
		.txtcolor-primary {color:<?php echo get_theme_mod('srg_theme_color_primary'); ?> !important;}
		.txtcolor-secondary {color:<?php echo get_theme_mod('srg_theme_color_secondary'); ?> !important;}
		
		.overlay.big	{border-color: transparent <?php echo get_theme_mod('srg_theme_color_primary'); ?> transparent transparent !important;line-height: 0px;_border-color: #000000 <?php echo get_theme_mod('srg_theme_color_primary'); ?> #000000 #000000;_filter: progid:DXImageTransform.Microsoft.Chroma(color='#000000');}
		.overlay.small {border-color: transparent transparent <?php echo get_theme_mod('srg_theme_color_primary'); ?>  transparent !important;line-height: 0px;_border-color: #000000 #000000 <?php echo get_theme_mod('srg_theme_color_primary'); ?>  #000000 !important;}
		.sidebar-row li a {color:<?php echo get_theme_mod('srg_theme_color_primary'); ?> !important; }
	</style>
    <?php 
 }
 add_filter('wp_head', 'srg_custom_style_options');

/*----------------------------------------------------------------------------------------------------*/

/**
 * Add analytics code to Footer
 * @uses get_option() to retrieve custom set color options
 */

function srg_footer_analytics(){
    echo get_theme_mod('srg_theme_analytics');
}
add_filter('wp_footer', 'srg_footer_analytics');
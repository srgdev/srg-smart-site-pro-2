<?php
/**
 * Template Name: Home Page News Feed
 */
 ?>

<?php get_header(); ?>
                
                <div class="main <?php echo is_active_sidebar('home-box') ? 'hasSidebar' : ''; ?>">
                                
               	<?php get_template_part('loop', 'news'); ?>
                
               <a class="see-all" href="<?php echo get_permalink(get_option('page_for_posts')); ?>">See All News</a>
                
                </div>
            
            <?php if(is_active_sidebar('home-box')): ?>
            	<div class="sidebar">
            	<?php dynamic_sidebar('home-box'); ?>
                </div>
            <?php endif; ?>

<?php get_footer(); ?>
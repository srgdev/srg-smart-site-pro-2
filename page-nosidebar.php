<?php
/**
* Template Name: Page no sidebar
*/
?>

<?php get_header(); ?>

			<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
                
                <div class="main">         
               		<article class="full-content">
				
						<?php if(has_post_thumbnail()): ?>
                            <div class="thumb">        
                                <?php the_post_thumbnail(); ?>  
                            </div>
                        <?php endif; ?>
                        
                         <h2 class="title txtcolor-primary"><?php echo (get_field('header')) ? get_field('header') : ''; ?></h2>
						<?php the_content(); ?>      
					</article>
                </div>
            
            <?php endwhile; ?>

<?php get_footer(); ?>
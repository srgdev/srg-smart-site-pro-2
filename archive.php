<?php get_header(); ?>

<?php if ( have_posts() ) the_post(); ?>

<h1 id="page-title">
<?php if ( is_day() ) : ?>
                <?php printf( __( 'Daily Archives: %s', 'twentyten' ), get_the_date() ); ?>
<?php elseif ( is_month() ) : ?>
                <?php printf( __( 'Monthly Archives: %s', 'twentyten' ), get_the_date('F Y') ); ?>
<?php elseif ( is_year() ) : ?>
                <?php printf( __( 'Yearly Archives: %s', 'twentyten' ), get_the_date('Y') ); ?>
<?php else : ?>
                <?php _e( 'Blog Archives', 'twentyten' ); ?>
<?php endif; ?>
</h1>

<?php rewind_posts(); ?>

<div class="main <?php echo is_active_sidebar('blog-sidebar') ? 'hasSidebar' : ''; ?>">

<?php get_template_part( 'loop', 'news' ); ?>

</div>

<?php if(is_active_sidebar('blog-sidebar')): ?>
	<div class="sidebar">
	<?php dynamic_sidebar('blog-sidebar'); ?>
    </div>
<?php endif; ?>

<?php get_footer(); ?>
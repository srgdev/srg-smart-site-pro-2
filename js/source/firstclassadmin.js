jQuery(document).ready(function($){
	console.log('loaded!');

	$('#step1').click(function(e){
		e.preventDefault();
		//data = $('#step1Form').serializeArray();
		data = {
			'clientID': $('#client').val(),
			'clientName': $('#clientName').val(),
			'step': $('#step1Form #step').val(),
			'action' : $('#action').val(),
			'clientIDName': $('#client option:selected').text()
		};
		console.log(data);
		if(data.clientName.length != 0){
			delete data.clientID;
			delete data.clientIDName;
		} else {
			delete data.clientName;
		}
		console.log(data);
		console.log(data.clientName);
		
		$.post(ajaxurl, data, function(response){
			console.log(response);
			if(response == '1'){
				$('#step1wrapper').slideUp();
				$('#step2wrapper').slideDown();
			}
		});

	});
	
	$('#step2').click(function(e){
		e.preventDefault();
		data = $('#step2Form').serialize();
		console.log(data);
		$.post(ajaxurl, data, function(response){
			console.log(response);
			if(response == '1'){
				location.reload();
			}
		});
	});
	
	$('#reset').click(function(e){
		e.preventDefault();
		data = {'reset': 'reset', 'action': 'firstclass_handler'};
		console.log(data);
		$.post(ajaxurl, data, function(response){
			console.log(response);
			if(response == '1'){
				location.reload();
			}
		});
	});
	
	function display_error(content){
		var container = $('#saved');
		var content = $('<div style="display:none;" id="message" class="error"></div>').html(content);
		container.html(content).find(content).slideDown();;
	};
	
	function remove_error(){
		$('#saved #message').slideUp('fast', function(){$('#saved #message').remove();});
	};
});

<?php get_header(); ?>

			<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
                <div class="main <?php echo is_active_sidebar('blog-sidebar') ? 'hasSidebar' : ''; ?>">           
               		<article class="full-content">
						<?php if(has_post_thumbnail()): ?>
                            <?php if(!in_category('multimedia')): ?>                       
                                <div class="thumb">                             
                                    <?php the_post_thumbnail(); ?>                                 
                                </div>                       
                            <?php endif; ?>                   
                        <?php endif; ?>           
                         <h2 class="title txtcolor-primary"><?php echo get_the_title(); ?></h2>         
                         <h3 class="date"><?php the_time('F j, Y') ?></h3>
						<?php if(in_category('multimedia')): ?>
                        	<?php $video_id = substr( get_field('youtube_link'), strpos(get_field('youtube_link'), '=')+1 ); ?>
                        	<div class="video-wrapper"><iframe width="600" height="360" class="video" src="//www.youtube.com/embed/<?php echo $video_id; ?>?rel=0" frameborder="0" allowfullscreen></iframe></div>
                        <?php endif; ?>
						<?php the_content(); ?>
					</article>  
                </div>
            <?php endwhile; ?>
			
            <?php if(is_active_sidebar('blog-sidebar')): ?>
            	<div class="sidebar">
            	<?php dynamic_sidebar('blog-sidebar'); ?>
                </div>
			<?php endif; ?>
            
<?php get_footer(); ?>
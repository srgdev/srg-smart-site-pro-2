<?php get_header(); ?>
    
	    <h1 id="page-title"><?php	printf( __( 'Category: %s', 'twentyten' ), '' . single_cat_title( '', false ) . '' );	?></h1>
    
    <div class="main <?php echo is_active_sidebar('blog-sidebar') ? 'hasSidebar' : ''; ?>">
    
    <?php get_template_part( 'loop', 'news' ); ?>
    
    </div>

<?php if(is_active_sidebar('blog-sidebar')): ?>
	<div class="sidebar">
	<?php dynamic_sidebar('blog-sidebar'); ?>
    </div>
<?php endif; ?>

<?php get_footer(); ?>
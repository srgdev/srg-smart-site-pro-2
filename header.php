<!DOCTYPE html>

<html <?php language_attributes(); ?>>

<head>

    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php wp_title( '|', true, 'right' );?></title>

    <link rel="profile" href="http://gmpg.org/xfn/11" />
    <link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
    <link href='http://fonts.googleapis.com/css?family=Droid+Serif:400,400italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'>
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">

    <!--[if lt IE 9]>

    <script src="<?php echo get_template_directory_uri(); ?>/js/html5shiv.js"></script>
    <link href="<?php echo get_template_directory_uri(); ?>/css/ie.css" rel="stylesheet" type="text/css">

    <![endif]-->

    <?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>

    <div id="fb-root"></div>
    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=428491740612913";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

    <header>
        <div class="wrapper">
            <div id="logo-wrapper">
                <a href="<?php echo home_url( '/' ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><img src="<?php echo get_theme_mod('srg_theme_logo') ? get_theme_mod('srg_theme_logo') : get_template_directory_uri().'/images/logo.png'; ?>" /></a>
            </div>

            <div id="header-menu-wrapper">
                <div class="menu">
                    <?php wp_nav_menu( array( 'theme_location' => 'main' ) ); ?>
                </div>

                <div class="social">
                    <?php if(get_theme_mod('srg_theme_tw')):?><a href="https://www.twitter.com/<?php echo get_theme_mod('srg_theme_tw'); ?>"><i class="fa fa-twitter bgcolor-primary"></i></a><?php endif; ?>
                    <?php if(get_theme_mod('srg_theme_fb')): ?><a href="<?php echo get_theme_mod('srg_theme_fb'); ?>"><i class="fa fa-facebook bgcolor-primary"></i></a><?php endif; ?>
                </div>

            </div>
            <br class="clear" />
        </div>
    </header>

     <div id="banner">

        <?php if(is_front_page()): ?>
            <?php if(get_theme_mod('srg_theme_showslider') == 1): ?>
                <?php $loop = new WP_Query(array('post_type' => 'slide', 'meta_key' => '_thumbnail_id', 'cat' => '-10', 'posts_per_page' => get_theme_mod('srg_theme_numslides'))); ?>
                <?php if($loop->have_posts()): ?>
                    <div class="slider">
                        <div class="slideShow cycle-slideshow" data-cycle-slides="> div" data-cycle-prev="#prev" data-cycle-next="#next" data-cycle-speed="2500" data-cycle-swipe="true">
                            <?php while ( $loop->have_posts() ) : $loop->the_post() ?>
                                <div class="slide pic">
                                    <?php $image =  wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
                                    <div class="header-image" style="background:url(' <?php echo $image[0] ?>') no-repeat center;background-size:cover;">
                                        <div class="overlay big"></div>
                                        <div class="overlay small"></div>
                                    </div>
                                    <div class="header-title bgcolor-primary">
                                        <?php $linked = get_field('link'); ?>
                                        <a href="<?php echo $linked;?>"><h1 class="title"><?php echo substr(get_the_title(), 0, 70).'...'; ?></h1></a>
                                        <div class="barWrapper">
                                            <span class="first bar bgcolor-secondary"></span>
                                            <span class="second bar"></span>
                                            <span class="second bar"></span>
                                            <br class="clear" />
                                        </div>
                                        <div class="sub-title"><?php echo substr(get_the_excerpt(), 0, 100).'...'; ?></div>
                                    </div>
                                </div>
                            <?php endwhile;  ?>
                        </div>
                        <div class="controls">
                            <div class="circle leftc" id="prev"><div class="fa fa-angle-left "></div></div>
                            <div class="circle rightc" id="next"><div class="fa fa-angle-right "></div></div>
                        </div>
                    </div>
                <?php else: ?>
                    <div class="noPosts">
                        <h1 class="noPostsTitle">Whoops!  You haven't added any slides yet!</h1>
                    </div>
                <?php endif; ?>
            <?php endif; ?>

        <?php else: ?>

                <div class="page-header">
                    <div class="header-image">
                        <div class="overlay big"></div>
                        <div class="overlay small"></div>
                        <div class="img" style="background:url(<?php generate_header_image(); ?>) no-repeat right top; background-size:cover;"><div class="gradient"></div></div>

                    </div>
                    <div class="header-title bgcolor-primary">
                       <h1 class="title"><?php header_title(); ?></h1>
                    </div>
                </div>

        <?php endif; ?>

    </div>
    <div id="content">
        <div class="wrapper">
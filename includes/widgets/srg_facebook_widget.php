<?php

/**
* SRG Facebook Widget - returnsfacebook likes iframe
*
* by: Calvin deClaisse-Walford
* v: 1.0
*
*/

class SRG_Facebook_Widget extends WP_Widget {

	function __construct() {
		parent::__construct('SRG_Facebook_Widget', 'SRG Facebook Widget', array( 'description' => 'Facebook Widget')	);
	}
	
	// Creating widget front-end
	// This is where the action happens
	public function widget( $args, $instance ) {
		
		$title = apply_filters( 'widget_title', $instance['title'] );

		echo $args['before_widget'];
		
		?>
        
        <?php if(get_theme_mod('srg_theme_fb')): ?>
        
         <h1 class="title"><?php echo $instance['title']; ?></h1>
        <div class="fb-like-box" data-href="<?php echo get_theme_mod('srg_theme_fb'); ?>" data-width="300px" data-height="400px" data-colorscheme="light" data-show-faces="true" data-header="false" data-stream="false" data-show-border="false"></div>
        <br class="clear" />
        <?php else: ?>
        
        <div class="noPosts">
        	<h1 class="noPostsTitle">Your Facebook page hasn't been entered in your options panel yet!</h1>
        </div>
        
        <?php endif; ?>
        
        <?php
		
		echo $args['after_widget'];
	}
			
	// Widget Backend 
	public function form( $instance ) {
		
		if ( isset( $instance[ 'title' ] ) ) {
			$title = $instance[ 'title' ];
		} else {
			$title = 'Stay Connected';
		}
		
		
		// Widget admin form
		?>
		<p>
		<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>
        
		<?php 
	}
		
	// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		return $instance;
	}

}

// Register and load the widget
function srg_facebook_widget_load() {
	register_widget( 'SRG_Facebook_Widget' );
}
add_action( 'widgets_init', 'srg_facebook_widget_load' );

?>
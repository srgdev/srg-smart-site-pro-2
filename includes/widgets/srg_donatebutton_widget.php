<?php

/**
* SRG Big Button Widget - Generates a simple button in a widget
*
* by: Calvin deClaisse-Walford
* v: 1.0
*
*/

class SRG_BigButton_Widget extends WP_Widget {

	function __construct() {
		parent::__construct('SRG_BigButton_Widget', 'SRG Big Button Widget', array( 'description' => 'Simple Button Widget')	);
	}
	
	// Creating widget front-end
	// This is where the action happens
	public function widget( $args, $instance ) {
		
		echo $args['before_widget'];

		?>
        <?php if(get_theme_mod('srg_theme_donatelink')):?>
        <a href="<?php echo get_theme_mod('srg_theme_donatelink'); ?>" class="killswitch"><input type="button" class="<?php echo get_theme_mod('srg_theme_button_coloropt'); ?>" id="district" value="<?php echo $instance['text']; ?>" /></a>
        <?php else: ?>
             <div class="noPosts">
            <h1 class="noPostsTitle">Your Donate page hasn't been entered in your options panel yet!</h1>
        </div>
        <?php endif; ?>
        <?php
		
		echo $args['after_widget'];
	}
			
	// Widget Backend 
	public function form( $instance ) {
		
		if ( isset( $instance[ 'text' ] ) ) {
			$text = $instance[ 'text' ];
		} else {
			$text = 'Donate';
		}
		
		// Widget admin form
		?>
		
        <p>
		<label for="<?php echo $this->get_field_id( 'text' ); ?>"><?php _e( 'Button Text:' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'text' ); ?>" name="<?php echo $this->get_field_name( 'text' ); ?>" type="text" value="<?php echo esc_attr( $text ); ?>" />
		</p>
 
		<?php 

	}
		
	// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['link'] = ( ! empty( $new_instance['link'] ) ) ? strip_tags( $new_instance['link'] ) : '';
		$instance['text'] = ( ! empty( $new_instance['text'] ) ) ? strip_tags( $new_instance['text'] ) : '';
		return $instance;
	}
	
}

// Register and load the widget
function srg_bigbutton_widget_load() {
	register_widget( 'SRG_BigButton_Widget' );
}
add_action( 'widgets_init', 'srg_bigbutton_widget_load' );

?>
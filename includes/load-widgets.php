<?php
/**
 * Load all widgets, disable WP widgets we don't need
 */
include(TEMPLATEPATH . '/includes/widgets/srg_donatebutton_widget.php');
include(TEMPLATEPATH . '/includes/widgets/srg_firstclass_widget.php');
include(TEMPLATEPATH . '/includes/widgets/srg_facebook_widget.php');
include(TEMPLATEPATH . '/includes/widgets/srg_tag_widget.php');

/*----------------------------------------------------------------------------------------------------*/

// unregister all widgets
function unregister_default_widgets() {
    unregister_widget('WP_Widget_Pages');
    unregister_widget('WP_Widget_Calendar');
    unregister_widget('WP_Widget_Links');
    unregister_widget('WP_Widget_Meta');
    unregister_widget('WP_Widget_Search');
    unregister_widget('WP_Widget_Text');
    unregister_widget('WP_Widget_Recent_Posts');
    unregister_widget('WP_Widget_Recent_Comments');
    unregister_widget('WP_Widget_RSS');
    unregister_widget('WP_Widget_Tag_Cloud');
    unregister_widget('WP_Nav_Menu_Widget');
    unregister_widget('Twenty_Eleven_Ephemera_Widget');
}
add_action('widgets_init', 'unregister_default_widgets', 11);

/*----------------------------------------------------------------------------------------------------*/
 
// Include CreateSendAPI wrapper
require_once TEMPLATEPATH.'/includes/createsend/class/base_classes.php';
if (!class_exists('Services_JSON')) {
    require_once TEMPLATEPATH.'/includes/createsend/class/services_json.php';
}
require_once TEMPLATEPATH.'/includes/createsend/class/serialisation.php';
require_once TEMPLATEPATH.'/includes/createsend/class/transport.php';
require_once TEMPLATEPATH.'/includes/createsend/class/log.php';
require_once TEMPLATEPATH.'/includes/createsend/csrest_lists.php';
require_once TEMPLATEPATH.'/includes/createsend/csrest_subscribers.php';
<?php
// Register the Slider Post Type
add_action( 'init', 'create_post_type' );
function create_post_type() {
    $labels = array(
               'name' => 'Slides',
        'singular_name' => 'Slide',
        'add_new' => 'Add new Slide',
        'add_new_item'=> 'Add new Slide',
        'edit_item' => 'Edit Slide',
        'new_item' => 'New Slide',
        'view_item' => 'View Slide',
        'search_items' => 'Search Slides',
        'not_found' => 'No Slides found',
        'not_found_in_trash' => 'No Slides found in Trash'
            );
    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'capability_type' => 'post',
        'has_archive' => true,
        'supports' => array( 'title', 'editor', 'author', 'thumbnail' )
      );
    register_post_type( 'slide', $args);
}
/*----------------------------------------------------------------------------------------------------*/
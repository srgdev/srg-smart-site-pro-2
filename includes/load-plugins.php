<?php
/**
 * Load packaged plugins, define some custom settings and globals
 * @uses srg_custom_fields()
 * @hook after_setup_theme
 */
function srg_plugin_loader(){
    define( 'ACF_LITE', true );
    include(TEMPLATEPATH . '/includes/plugins/advanced-custom-fields/acf.php');
    include(TEMPLATEPATH . '/includes/plugins/acf-repeater/acf-repeater.php');
    include(TEMPLATEPATH . '/includes/plugins/add-custom-post-types-archive-to-nav-menus/cpt-in-navmenu.php');
    
    // Setup custom fields
    srg_custom_fields();
}
add_action( 'after_setup_theme', 'srg_plugin_loader' );

/*----------------------------------------------------------------------------------------------------*/

/**
* Register custom fields
*/
function srg_custom_fields(){
    
    register_field_group(array (
      'id' => '53444d7598b0a',
      'title' => 'Header Intro Text for Pages',
      'fields' => 
      array (
        0 => 
        array (
          'key' => 'field_52c57c43b4263',
          'label' => 'Title/Intro Text',
          'name' => 'header',
          'type' => 'text',
          'instructions' => 'Blue title text that appears at the top of the article/page',
          'required' => '0',
          'default_value' => '',
          'formatting' => 'html',
          'order_no' => 0,
        ),
      ),
      'location' => 
      array (
        'rules' => 
        array (
          0 => 
          array (
            'param' => 'page_template',
            'operator' => '==',
            'value' => 'default',
            'order_no' => 0,
          ),
          1 => 
          array (
            'param' => 'post_type',
            'operator' => '==',
            'value' => 'page',
            'order_no' => 1,
          ),
        ),
        'allorany' => 'all',
      ),
      'options' => 
      array (
        'position' => 'normal',
        'layout' => 'default',
        'show_on_page' => 
        array (
          0 => 'the_content',
          1 => 'custom_fields',
          2 => 'discussion',
          3 => 'comments',
          4 => 'slug',
          5 => 'author',
        ),
      ),
      'menu_order' => 0,
    ));
    register_field_group(array (
      'id' => '53444d75996bb',
      'title' => 'Multimedia Youtube Embed',
      'fields' => 
      array (
        0 => 
        array (
          'key' => 'field_52c6e950438b2',
          'label' => 'Youtube link',
          'name' => 'youtube_link',
          'type' => 'text',
          'instructions' => 'Paste the address for the Youtube video here (http://www.youtube.com/watch...etc) - we\'ll handle the rest!',
          'required' => '1',
          'default_value' => '',
          'formatting' => 'html',
          'order_no' => 0,
        ),
        1 => 
        array (
          'label' => 'Show YouTube thumbnail?',
          'name' => 'youtube_thumb',
          'type' => 'checkbox',
          'instructions' => 'Check this box if you want to use the thumbnail from YouTube as the featured image',
          'required' => '0',
          'choices' => 
          array (
            'Yes' => 'Yes',
          ),
          'key' => 'field_52d03f4de01fd',
          'order_no' => 1,
        ),
      ),
      'location' => 
      array (
        'rules' => 
        array (
          0 => 
          array (
            'param' => 'post_category',
            'operator' => '==',
            'value' => '10',
            'order_no' => 0,
          ),
        ),
        'allorany' => 'all',
      ),
      'options' => 
      array (
        'position' => 'normal',
        'layout' => 'default',
        'show_on_page' => 
        array (
          0 => 'the_content',
          1 => 'custom_fields',
          2 => 'discussion',
          3 => 'comments',
          4 => 'slug',
          5 => 'author',
        ),
      ),
      'menu_order' => 0,
    ));
    register_field_group(array (
      'id' => '53444d759a274',
      'title' => 'Slider links',
      'fields' => 
      array (
        0 => 
        array (
          'label' => 'Link',
          'name' => 'link',
          'type' => 'text',
          'instructions' => 'Paste the page you would like to link the slide to here.',
          'required' => '1',
          'default_value' => '/',
          'formatting' => 'html',
          'key' => 'field_52d405d7dade9',
          'order_no' => 0,
        ),
      ),
      'location' => 
      array (
        'rules' => 
        array (
          0 => 
          array (
            'param' => 'post_type',
            'operator' => '==',
            'value' => 'slide',
            'order_no' => 0,
          ),
        ),
        'allorany' => 'all',
      ),
      'options' => 
      array (
        'position' => 'normal',
        'layout' => 'default',
        'show_on_page' => 
        array (
          0 => 'the_content',
          1 => 'custom_fields',
          2 => 'discussion',
          3 => 'comments',
          4 => 'slug',
          5 => 'author',
        ),
      ),
      'menu_order' => 0,
    ));
  register_field_group(array (
    'id' => 'acf_header-images',
    'title' => 'Header Images',
    'fields' => array (
      array (
        'key' => 'field_53a07f4285f46',
        'label' => 'Header Image',
        'name' => 'header_image',
        'type' => 'image',
        'instructions' => 'Add header image.',
        'save_format' => 'url',
        'preview_size' => 'thumbnail',
        'library' => 'all',
      ),
    ),
    'location' => array (
      array (
        array (
          'param' => 'post_type',
          'operator' => '==',
          'value' => 'page',
          'order_no' => 0,
          'group_no' => 0,
        ),
      ),
    ),
    'options' => array (
      'position' => 'normal',
      'layout' => 'no_box',
      'hide_on_screen' => array (
      ),
    ),
    'menu_order' => 0,
  ));

};
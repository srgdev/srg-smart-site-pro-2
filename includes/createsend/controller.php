<?php
// Load our WP environment so we can use options and functions
define('WP_USE_THEMES', false);
require_once('../../../../../wp-load.php');

if($_GET){

	// store all our values
	$name = strip_tags($_GET['name']) ? strip_tags($_GET['name']) : NULL;
	$email = strip_tags($_GET['email']) ? strip_tags($_GET['email']) : NULL;
	$zip = strip_tags($_GET['zip']) ? strip_tags($_GET['zip']) : NULL;
	$listID = get_option('srg_csrest_listid');
	
	if($name && $email && $zip && $listID) {

		$auth = array('api_key' => 'bb6d4dc56e8006e41a8b7be1c86fc898');
		$wrap = new CS_REST_Subscribers($listID, $auth);
		$result = $wrap->add(array(
			'EmailAddress' => $email,
			'Name' => $name,
			'CustomFields' => array(
				array(
					'Key' => 'ZIP',
					'Value' => $zip
				)
			),
			'Resubscribe' => true
		));

		if($result->was_successful()) {
			echo json_encode(array('Success', $result->http_status_code));
		} else {
			echo json_encode(array('Error', $reult->http_status_code));
		}
	} else {
		
		echo json_encode(array('Error', 'Some value definitions are missing'));
	}

} else {
	echo json_encode(array('Error', 'No values defined'));	
}

?>
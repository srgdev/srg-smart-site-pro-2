<?php
/**
* Register Sidebars
* @ uses register_sidebar()
*/
function srg_sidebars_init() {
        register_sidebar( array(
            'name' => __( 'Home Page Sidebar', 'srg' ),
            'id' => 'home-box',
            'description' => __( 'The home page sidebar area', 'srg' ),
            'before_widget' => '<div class="sidebar-row">',
            'after_widget' => '</div>',
            'before_title' => '<h1 class="sub-title">',
            'after_title' => '</h1>',
        ) );
        register_sidebar( array(
            'name' => __( 'Blog Sidebar', 'srg' ),
            'id' => 'blog-sidebar',
            'description' => __( 'The blog page and posts sidebar', 'srg' ),
            'before_widget' => '<div class="sidebar-row">',
            'after_widget' => '</div>',
            'before_title' => '<h1 class="sub-title">',
            'after_title' => '</h1>',
        ) );
        register_sidebar( array(
            'name' => __( 'Single Page Sidebar', 'srg' ),
            'id' => 'page-sidebar',
            'description' => __( 'The single page sidebar', 'srg' ),
            'before_widget' => '<div class="sidebar-row">',
            'after_widget' => '</div>',
            'before_title' => '<h1 class="sub-title">',
            'after_title' => '</h1>',
        ) );       
}
// Register sidebars in init
add_action( 'widgets_init', 'srg_sidebars_init' );

/*----------------------------------------------------------------------------------------------------*/ 
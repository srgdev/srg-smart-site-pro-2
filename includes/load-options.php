<?php
/**
 * Register our custom theme options
 * @param $wp_customize
 * @uses add_setting()
 * @uses add_section()
 * @uses add_control()
 */
function srg_customize_register($wp_customize) {

    require_once(TEMPLATEPATH . '/includes/classes/WP_Customize_Control_Textarea.php');

    // Add settings from registered options
    $wp_customize->add_setting( 'srg_theme_logo', array('default' => false,'type' => 'theme_mod') );
    $wp_customize->add_setting( 'srg_theme_logo_footer', array('default' =>false,'type' => 'theme_mod') );
    $wp_customize->add_setting( 'srg_theme_color_primary', array('default' => '#235796','type' => 'theme_mod') );
    $wp_customize->add_setting( 'srg_theme_color_secondary', array('default' => '#ff0000','type' => 'theme_mod') );
    $wp_customize->add_setting( 'srg_theme_color_banner', array('default' => '#235796','type' => 'theme_mod') );
    $wp_customize->add_setting( 'srg_theme_button_coloropt', array('default' => 'bgcolor-primary','type' => 'theme_mod') );
    $wp_customize->add_setting( 'srg_theme_fb', array('default' => false,'type' => 'theme_mod') );
    $wp_customize->add_setting( 'srg_theme_tw', array('default' => false,'type' => 'theme_mod') );
    $wp_customize->add_setting( 'srg_theme_paidfor', array('default' => false,'type' => 'theme_mod') );
    $wp_customize->add_setting( 'srg_theme_donatelink', array('default' => false,'type' => 'theme_mod') );
    $wp_customize->add_setting( 'srg_theme_showslider', array('default' => false,'type' => 'theme_mod') );
    $wp_customize->add_setting( 'srg_theme_numslides', array('default' => false,'type' => 'theme_mod') );
    // Analytics
    $wp_customize->add_setting( 'srg_theme_analytics', array(	'default' => false,'type' => 'theme_mod') );

    // Add options sections
    $wp_customize->add_section( 'srg_header_options', array('title' => 'Header Options','priority' => 35) );
    $wp_customize->add_section( 'srg_banner_options', array('title' => 'Slider Options','priority' => 36) );
    $wp_customize->add_section( 'srg_social_options', array('title' => 'Social/General Options','priority' => 37) );
    $wp_customize->add_section( 'srg_color_options', array( 'title' => 'Color Options','priority' => 38) );
    $wp_customize->add_section( 'srg_footer_options', array('title' => 'Footer Options','priority' => 39 ) );

    // Add controls to our options
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'header_image', array('label'   => 'Header Image','section' => 'srg_header_options','settings'   => 'srg_theme_logo',) ) );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'footer_image', array('label'   => 'Footer Image','section' => 'srg_footer_options','settings'   => 'srg_theme_logo_footer', ) ) );
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'primary_color', array('label'   => 'Primary Color','section' => 'srg_color_options','settings'   => 'srg_theme_color_primary',) ) );
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'secondary_color', array('label'   => 'Secondary Color','section' => 'srg_color_options','settings'   => 'srg_theme_color_secondary', ) ) );
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'banner_color', array('label'   => 'Banner Color','section' => 'srg_banner_options','settings'   => 'srg_theme_color_banner', ) ) );
    $wp_customize->add_control( 'button_color', array('label' => __( 'Button Widget Color', 'themename' ),'section'    => 'srg_color_options','settings'   => 'srg_theme_button_coloropt','type' => 'radio','choices'=> array('bgcolor-primary' => 'Primary','bgcolor-secondary' => 'Secondary', ), ) );
    $wp_customize->add_control( 'srg_theme_fb', array('label' => __( 'Facebook Page URL'), 'section' => 'srg_social_options'));
    $wp_customize->add_control( 'srg_theme_tw', array('label' => __( 'Twitter Handle'), 'section' => 'srg_social_options'));
    $wp_customize->add_control( 'srg_theme_donatelink', array('label' => __( 'Donate Link'), 'section' => 'srg_social_options'));
    $wp_customize->add_control( 'srg_theme_paidfor', array('label' => __( 'Paid for by...'), 'section' => 'srg_footer_options'));
     // Slider opts
    $wp_customize->add_control( 'srg_theme_numslides', array('label' => __( 'Number of slides' ),'section' => 'srg_banner_options',) );
    $wp_customize->add_control( new WP_Customize_Control($wp_customize,'showslider', array('label' => __( 'Show the slider?', 'srg' ),'section'=> 'srg_banner_options','settings' => 'srg_theme_showslider','type' => 'checkbox') ) );
    // Analytics
    $wp_customize->add_control( new WP_Customize_Control_Textarea( $wp_customize, 'srg_theme_analytics', array('label' => __( 'Google Analytics Code'), 'section' => 'srg_social_options', 'settings' => 'srg_theme_analytics')));
}
add_action( 'customize_register', 'srg_customize_register' );

/*----------------------------------------------------------------------------------------------------*/

/**
 * Check options that contain urls and ensure they are on the local domain
 */
function check_options_urls(){

    $current_url = get_bloginfo('url');

    $opts = array();
    $opts['srg_theme_logo'] = get_theme_mod('srg_theme_logo');
    $opts['srg_theme_logo_footer'] = get_theme_mod('srg_theme_logo_footer');

    // Loop
    foreach($opts as $name=>$opt){

        // If it has been set
        if($opt != ''){

            // Figure out what the old url was, so we can strip it out if it is different
            $old_url = substr($opt, 0, strpos($opt, '/wp-content')) . '<br>';

            // If they dont match
            if($old_url != $current_url){

                // Get the wp-content path
                $path = substr($opt, strpos($opt, '/wp-content'), strlen($opt));

                // Add current url to beginning
                $path = $current_url . $path;

                // Set the theme mod
                set_theme_mod($name, $path);
            }
        }
    }
}
add_action('init', 'check_options_urls');
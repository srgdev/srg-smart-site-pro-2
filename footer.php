		 <br class="clear" />
		</div>    
    </div>
    
    <footer>
    	<div class="wrapper">
        	<div id="logo-wrapper-footer">
            	<a href="<?php echo home_url( '/' ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><img src="<?php echo get_theme_mod('srg_theme_logo_footer', get_template_directory_uri().'/images/logo-footer.png'); ?>" /></a>
            </div>
            
             <div id="footer-menu-wrapper">
            	<div class="menu">
                	<?php wp_nav_menu( array( 'theme_location' => 'main', 'depth' => 1 ) ); ?>
                </div>

                <div class="social">
                	<?php if(get_theme_mod('srg_theme_tw')):?><a href="<?php echo get_theme_mod('srg_theme_tw'); ?>"><i class="fa fa-twitter"></i></a><?php endif; ?>
                    <?php if(get_theme_mod('srg_theme_fb')): ?><a href="<?php echo get_theme_mod('srg_theme_fb'); ?>"><i class="fa fa-facebook"></i></a><?php endif; ?>
                </div>
            
            </div>
            
            <div id="footer-credentials">
                <a id="SRG" href="http://www.stoneridgegroup.com"><img src="<?php echo get_template_directory_uri(); ?>/images/srg.png" /></a>
                <?php if(get_theme_mod('srg_theme_paidfor')):?><span id="paid-for"><?php echo get_theme_mod('srg_theme_paidfor'); ?></span><?php endif; ?>
            </div>
            
            <br class="clear" />
            
        </div>
    </footer>

<?php wp_footer(); ?>

</body>
</html>